import React, { Component } from 'react';
import { View, StatusBar, Text, TouchableOpacity, StyleSheet } from 'react-native';

class App extends Component {
  constructor(props) {
    super (props);
    this.state = {
        hasil: 0,
        hitung: 0,
      };
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#000000'}}>
        <StatusBar backgroundColor="black" barStyle="light-content" />

        <View style={styles.kotak}>
          <Text style={styles.teksH}>Kalkulator</Text>
        </View>


        <View style={{flex: 0.9, justifyContent: 'center', marginHorizontal: 10}}>
          <Text style={{color: '#FFFFFF', fontSize: 50, textAlign: 'right', color: '#ffffff'}}>0</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10, margin: 10 }}>
            <Text style={{ color: '#FFFFFF', fontSize: 24, textAlign: 'right', color: '#ff0000' }}>Clear</Text>
           </TouchableOpacity>
       </View>
        
        <View style={{flexDirection: 'row', marginVertical: 10,}}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10 }}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>(</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>/</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>X</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>7</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>8</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>9</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>+</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>4</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>5</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>6</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>-</Text>
          </TouchableOpacity>
        </View>
        
        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>2</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center'}}>3</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#FFFFFF', fontSize: 24, textAlign: 'center', color: '#ffff54'}}>=</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}

export default App


const styles = StyleSheet.create({
  kotak:{ 
    backgroundColor: '#9e9e9e',
  },
  teksH:{
    color: '#ffff54', 
    fontSize: 50, 
    marginHorizontal: 30,
    fontWeight: 'bold', 
  },
  teksS:{
    color: '#ffffff', 
    fontSize: 25,
    top: -10,
    marginHorizontal: 150,
  }
})

